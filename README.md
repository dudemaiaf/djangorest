# djangoRest

# Passo 1:
Criar a maquina virtual para rodar o projeto e baixar as ferramentas

# Passo 2:
"pip3 install django djangorestframework coreapi"

# Passo 3:
Entrar na pasta do projeto em si (authapi)
"cd authapi/"

# Passo 4:
Comando para criar as migrations
"python3 manage.py migrate"

e se quiser criar super usuario
"python3 manage.py createsuperuser"

# Passo 5:
Comando pra rodar projeto
"python3 manage.py runserver"
